<!DOCTYPE html>
<html lang="en">
<head>
    <title>Universal biologicals - Life Science Research Products</title>
    <meta charset="utf-8">
    <meta name="author" content="">
	<meta name="description" content=""/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="apple-touch-icon" href="apple-touch-icon.png" /><!-- 57×57px -->
    <link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png"><!-- 180×180px -->

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
    
    <!-- Facebook Open Graph -->
    <meta property="og:locale" content="en-EN">
    <meta property="og:type" content="website">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta property="og:url" content="">
    <meta property="og:site_name" content="">
    
    <!-- Twitter Card -->
    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:creator" content="">
    <meta name="twitter:url" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">
    <meta name="twitter:image:alt" content="">
    

</head>

<body>

	<header role="banner">
        
        <div class="top-bar">
            <div class="features">
                <ul class="">
                    <li><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-lock"></use></svg><span>100% Secure Onine Payments</span></li>
                    <li><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-truck"></use></svg><span>Fast & Efficient Delivery</span></li>
                    <li><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-percent"></use></svg><span>Bulk DISCOUNTS Available</span></li>
                </ul>
            </div>
            
            <div class="shop">
                <nav class="shop-nav">
                    <ul class="shop-menu">
                        <li>
                            <a href="#" class="" title="Info">
                                <svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-info"></use></svg>
                                <span class="label">Info</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="" title="Customer Support">
                                <svg class="icon comment"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-comment"></use></svg>
                                <span class="label">Customer Support</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="login" title="My Account">
                                <svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-user"></use></svg>
                                <span class="label">My Account</span>
                            </a>
                        </li>
                    </ul>
                </nav>
                
                <div class="shop-options">
                    <!-- Currency switcher -->
                    <div class="currency" id="currency-switcher">
                        <ul class="currency-chooser">
                            <li class="active"><a href="#" title="GBP" class="currency_text"><span>£</span> GBP</a></li>
                            <li class=""><a href="#" title="EUR" class="currency_text"><span>€</span> EUR</a></li>
                            <li class=""><a href="#" title="USD" class="currency_text"><span>$</span> USD</a></li>
                        </ul>
                    </div>

                    <!-- Cart box -->
                    <div class="cart-box">
                        <a href="#" rel="modal:open" class="cart" title="Cart">
                            <svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-cart"></use></svg>
                            <span class="label">Cart</span>
                        </a>

                        <div class="woo-items-count">
                            <a class="cart-contents" href="#" title="Your Shopping Cart">
                                <span class="cart-contents-count">2</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="header-wrapper">
            <div class="logo">
                <a href="#" rel="home">
                    <img src="assets/images/universal-biolobicals-logo@2x.png" alt="Universal Biologicals logo" width="175" height="63">
                </a>
            </div>

            <div class="underline">
                <nav role="navigation">
                    <ul class="main-menu">
                        <li class="active"><a href="#">Products</a></li>
                        <li><a href="#">Suppliers</a></li>
                        <li><a href="#">News</a></li>
                    </ul>
                </nav>

                <div class="site-search">
                    <form role="search" method="get" class="product-search" action="">
                        <input type="search" id="woocommerce-product-search-field-0" class="search-field" placeholder="Search" value="" name="s">
                        <button type="submit" value="Buscar">
                            <svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-search"></use></svg>
                        </button>
                        <input type="hidden" name="post_type" value="product">
                    </form>
                </div>

                <div class="call">
                    <p>Order online or call <a href="tel:01480839015">01480 839 015</a></p>
                </div>
            </div>
        </div><!-- /header-wrapper -->
	</header>