<?php include("header.php"); ?>


<section class="billboard">
    <img src="assets/images/Depositphotos_152093816_xl-2015@2x.png" alt="Universal Biologicals" width="2882" height="340">
    
    <div class="row">
        <div class="col-6">
            <div class="col-content billboard-left">
                <h1>Medium length display headline</h1>
                <p>Nam sodales tellus at lacinia fermentum. Maecenas congue nisi elit, quis sagittis magna faucibus a. Nunc mollis eros id ligula venenatis</p>
                <a href="#">Learn more</a>
            </div>
        </div>
        
        <div class="col-6">
            <div class="col-content billboard-right">
                <div class="box-search">
                    <h2>Search</h2>
                    <form role="search" method="get" class="main-search" action="">
                        <input type="search" id="woocommerce-product-search-field-0" class="search-field" placeholder="Search keywords" value="" name="s">
                        <a href="#">Advanced search</a>
                        <button type="submit" value="Advanced search">Search
                        </button>
                        <input type="hidden" name="post_type" value="product">
                    </form>
                </div>
                <div class="diamonds">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
    </div>
</section>


<main>
    
    <section class="container suppliers">
        <h2>Trusted suppliers of</h2>
    </section>
    
</main>


<?php include("footer.php"); ?>